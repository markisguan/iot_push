package com.lxr.iot.util;

import org.apache.commons.lang3.ObjectUtils;

/**
 * @author : guanjing
 * @since : 2018/9/26
 */
public class ObjectUtil extends ObjectUtils {

    public static boolean allNotNull(Object... values) {
        if (values == null) {
            return false;
        } else {
            Object[] var1 = values;
            int var2 = values.length;

            for(int var3 = 0; var3 < var2; ++var3) {
                Object val = var1[var3];
                if (val == null) {
                    return false;
                }
            }

            return true;
        }
    }
}
